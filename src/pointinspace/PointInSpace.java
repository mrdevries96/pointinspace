/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pointinspace;

/**
 *
 * @author mrdevries
 */
public class PointInSpace {
    /**
     * the coordinates
     */
    int x;
    int y;
    int z;
    /**
     * Calculates Euclidian distance to other points in space
     * @param otherPoint
     * @return distance rounded down
     */
    int calculateDistance(PointInSpace otherPoint) {
        int distance = 0;
        distance = (int) Math.sqrt(
            Math.pow(this.x - otherPoint.x, 2)
            + Math.pow(this.y - otherPoint.y, 2)
            + Math.pow(this.z - otherPoint.z, 2)
        );
        return distance;

    }

}
