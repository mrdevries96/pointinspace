/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pointinspace;
/**
 *
 * @author mrdevries
 */
public class SpaceTester {
    public static void main(final String[] args) {
        SpaceTester mainObject = new SpaceTester();
        mainObject.start();
    }

    private void start() {
        PointInSpace p1 = new PointInSpace();
        p1.x = 10;
        p1.y = 3;
        p1.z = 1;
        PointInSpace p2 = new PointInSpace();
        p2.x = 8;
        p2.y = 5;
        p2.z = 2;
        System.out.println(p1.calculateDistance(p2));
    }
}
